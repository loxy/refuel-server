import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { DbService } from './db/db.service';
import { AuthcModule } from './authc/authc.module';
import { VehicleModule } from './vehicle/vehicle.module';
import { FuelstopModule } from './fuelstop/fuelstop.module';

@Module({
  imports: [AuthcModule, VehicleModule, FuelstopModule],
  controllers: [AppController],
  providers: [],
})
export class AppModule {}
