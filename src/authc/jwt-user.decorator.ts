import { createParamDecorator } from '@nestjs/common';

export interface IJwtUser {
  userId: string;
  role: string;
}

export const JwtUser = createParamDecorator((data, req) => {
  return req.user;
});
