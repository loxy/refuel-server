import { Module } from '@nestjs/common';
import { PassportModule } from '@nestjs/passport';
import { JwtModule } from '@nestjs/jwt';

import { AuthcService } from './authc.service';
import { JwtStrategy } from './jwt.strategy';
import { JWT } from '../common/constants';
import { UserService } from './user.service';
import { LocalStrategy } from './local.strategy';

@Module({
  imports: [
    PassportModule.register({ defaultStrategy: 'jwt' }),
    JwtModule.register({
      secret: JWT.secret,
      signOptions: { expiresIn: '60m' },
    }),
  ],
  exports: [AuthcService],
  providers: [JwtStrategy, LocalStrategy, AuthcService, UserService],
})
export class AuthcModule {}
