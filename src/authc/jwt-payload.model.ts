export interface JwtPayload {
  sub: string;
  name?: string;
  role?: string;
  iat: number;
  exp: number;
}
