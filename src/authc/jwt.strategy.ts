import { Injectable } from '@nestjs/common';
import { PassportStrategy } from '@nestjs/passport';
import { ExtractJwt, Strategy } from 'passport-jwt';
import { JwtPayload } from './jwt-payload.model';
import { JWT } from '../common/constants';

/**
 * We are protecting our endpoints by requiring a valid JWT be present on the request.
 * Passport can help us here too. It provides the `passport-jwt` strategy for securing RESTful endpoints with JSON Web Tokens.
 */
@Injectable()
export class JwtStrategy extends PassportStrategy(Strategy) {
  constructor() {
    // This strategy requires some initialization, so we do that by passing in an options object in the super() call.
    // You can read more about the available options here:
    // https://github.com/mikenicholson/passport-jwt#configure-strategy
    super({
      jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
      algorithms: ['HS256'],
      ignoreExpiration: false,
      secretOrKey: JWT.secret,
    });
  }

  /**
   * For the jwt-strategy, Passport first verifies the JWT's signature and decodes the JSON.
   * It then invokes our validate() method passing the decoded JSON as its single parameter.
   * Based on the way JWT signing works, we're guaranteed that we're receiving a valid token that we have previously signed and issued to a valid user.
   * @param payload
   */
  async validate(payload: JwtPayload) {
    const { name, role } = payload;
    return { name, userId: payload.sub, role };
  }
}
