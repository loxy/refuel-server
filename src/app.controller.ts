import { Request, Controller, Get, Post, UseGuards } from '@nestjs/common';
import { AuthcService } from './authc/authc.service';
import { AuthGuard } from '@nestjs/passport';

@Controller()
export class AppController {
  constructor(private readonly authService: AuthcService) {}

  @Get()
  getHello(): string {
    return 'Hello World!';
  }

  @UseGuards(AuthGuard('local'))
  @Post('api/login')
  async login(@Request() req) {
    return this.authService.login(req.user);
  }
}
