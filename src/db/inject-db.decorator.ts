import { Inject } from '@nestjs/common';
import { PROVIDE } from '../common/constants';

export function InjectDb() {
  return Inject(PROVIDE.DB);
}
