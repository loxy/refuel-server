import { Module } from '@nestjs/common';
import { DbService } from './db.service';

const instance = new DbService().instance;

@Module({
  imports: [],
  exports: [
    {
      provide: 'DB',
      useValue: instance,
    },
  ],
  providers: [
    {
      provide: 'DB',
      useValue: instance,
    },
  ],
})
export class DbModule {}
