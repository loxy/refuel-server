import low from 'lowdb';

import { Fuelstop } from '../fuelstop/fuelstop.model';
import { Vehicle } from '../vehicle/vehicle.model';

export interface DbSchema {
  vehicles: Vehicle[];
  stops: Fuelstop[];
}

export type LowDb = low.LowdbSync<DbSchema>;
