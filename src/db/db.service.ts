import low, { AdapterSync } from 'lowdb';
import FileSync from 'lowdb/adapters/FileSync';
import { DbSchema } from './db-schema.model';

export class DbService {
  private readonly adapter: AdapterSync;
  private readonly db: low.LowdbSync<DbSchema>;

  constructor(file: string = 'db') {
    this.adapter = new FileSync(`db/${file}.json`);
    this.db = low(this.adapter);
    this.db.defaults({ vehicles: [], stops: [] }).write();
  }

  get instance() {
    return this.db;
  }
}
