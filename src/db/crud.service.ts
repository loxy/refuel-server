import { generateId } from '../common/generate-id.fn';

export class CrudService<T extends { id: string }> {
  constructor(
    protected readonly db: any,
    protected readonly tableName: string,
  ) {}

  getById(id: string): T {
    return this.db
      .get(this.tableName)
      .find({ id })
      .value();
  }

  getAll(): T[] {
    return this.db.get(this.tableName).value();
  }

  addAll(list: T[]) {
    list = list.map(e => {
      e.id = e.id || generateId();
      return e;
    });
    this.db
      .get(this.tableName)
      .push(...list)
      .write();
  }

  add(entry: T): T {
    entry.id = entry.id || generateId();
    this.db
      .get(this.tableName)
      .push(entry)
      .write();
    return entry;
  }

  updateById(id: string, data: Partial<T>): T {
    return this.db
      .get(this.tableName)
      .find({ id })
      .assign(data)
      .write();
  }

  removeById(id: string): { removed: string } {
    this.db
      .get(this.tableName)
      .remove({ id })
      .write();
    return { removed: id };
  }

  removeAll(): void {
    this.db.set(this.tableName, []).write();
  }
}
