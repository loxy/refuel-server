export const PROVIDE = {
  DB: 'DB',
};

export const TABLE = {
  VEHICLES: 'vehicles',
  STOPS: 'stops',
};

export const JWT = {
  secret: 'secretKey',
};
