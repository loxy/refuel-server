import generate from 'nanoid/generate';

const alphabet =
  '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz';

export function generateId(length = 12) {
  return generate(alphabet, length); //=> "h9s9nkYrTVwl"
}
