import { generateRandomNumberComplete as sut } from './generate-random-number.fn';

describe('generateRandomNumber', () => {
  it('should work somehow', () => {
    const random = sut(1, 2);
    expect(random.number).toBeGreaterThanOrEqual(1);
    expect(random.number).toBeLessThanOrEqual(2);
    expect(random.min).toEqual(1);
    expect(random.max).toEqual(2);
  });

  it('should work for strings', () => {
    const random = sut('1' as any, '2' as any);
    expect(random.number).toBeGreaterThanOrEqual(1);
    expect(random.number).toBeLessThanOrEqual(2);
    expect(random.min).toEqual(1);
    expect(random.max).toEqual(2);
  });

  it('should use useful defaults', () => {
    const random = sut('min' as any, 'max' as any);
    expect(random.number).toBeGreaterThanOrEqual(0);
    expect(random.number).toBeLessThanOrEqual(Number.MAX_SAFE_INTEGER);
    expect(random.min).toEqual(0);
    expect(random.max).toEqual(Number.MAX_SAFE_INTEGER);
  });

  it('should work if min === max', () => {
    const random = sut(4, 4);
    expect(random.number).toEqual(4);
    expect(random.min).toEqual(4);
    expect(random.max).toEqual(4);
  });

  it('should throw if min > max', () => {
    expect(() => sut(2, 1)).toThrow();
  });

  it('should work for one negative numbers', () => {
    const random = sut(-110, 0);
    expect(random.number).toBeGreaterThanOrEqual(-110);
    expect(random.number).toBeLessThanOrEqual(0);
  });

  it('should work for both negative numbers', () => {
    const random = sut(-2, -1);
    expect(random.number).toBeGreaterThanOrEqual(-2);
    expect(random.number).toBeLessThanOrEqual(-1);
  });

  describe('when random value is mocked', () => {
    afterEach(() => {
      (global.Math.random as jest.Mock).mockRestore();
    });

    describe('with 0 (low bound)', () => {
      beforeEach(() => {
        jest.spyOn(global.Math, 'random').mockReturnValue(0);
      });

      it('should work for one negative numbers', () => {
        expect(sut(-1, 2).number).toEqual(-1);
        expect(sut(-1, 0).number).toEqual(-1);
      });

      it('should work for both negative numbers', () => {
        expect(sut(-2, -1).number).toEqual(-2);
      });
    });

    describe('with 0.123 (somewhere in the middle)', () => {
      beforeEach(() => {
        jest.spyOn(global.Math, 'random').mockReturnValue(0.123);
      });

      it('should equal 0.123 on identity bounds [0, 1]', () => {
        expect(sut(0, 1).number).toEqual(0.123);
      });
    });

    describe('with 1 (high bound)', () => {
      beforeEach(() => {
        jest.spyOn(global.Math, 'random').mockReturnValue(1);
      });

      it('should work for one negative numbers', () => {
        expect(sut(-1, 2).number).toEqual(2);
        expect(sut(-1, 0).number).toEqual(0);
      });

      it('should work for both negative numbers', () => {
        expect(sut(-2, -1).number).toEqual(-1);
      });
    });
  });
});
