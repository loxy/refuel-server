export function generateRandomIntComplete(min: number, max: number) {
  min = +min;
  max = +max;

  if (isNaN(min)) {
    min = 0;
  }

  if (isNaN(max)) {
    max = Number.MAX_SAFE_INTEGER;
  }

  if (min > max) {
    throw new Error('<min> must be smaller than <max>');
  }

  const n = Math.floor(min + Math.random() * (max + 1 - min));

  return {
    min,
    max,
    number: n,
  };
}

export function generateRandomInt(min: number, max: number) {
  return generateRandomIntComplete(min, max).number;
}
