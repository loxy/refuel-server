import { Injectable } from '@nestjs/common';

import { TABLE } from '../common/constants';
import { LowDb } from '../db/db-schema.model';
import { InjectDb } from '../db/inject-db.decorator';
import { Vehicle } from './vehicle.model';
import { CrudService } from '../db/crud.service';

@Injectable()
export class VehicleService extends CrudService<Vehicle> {
  constructor(@InjectDb() db: LowDb) {
    super(db, TABLE.VEHICLES);
  }

  updateById(id: string, data: Partial<Vehicle>): Vehicle {
    if (data.default) {
      this.db
        .get(this.tableName)
        .filter({ default: true })
        .each(item => {
          item.default = false;
        })
        .write();
    }
    return super.updateById(id, data);
  }
}
