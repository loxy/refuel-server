import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Patch,
  Post,
} from '@nestjs/common';
import { VehicleService } from './vehicle.service';
import { Vehicle } from './vehicle.model';

@Controller('/api/vehicles')
export class VehicleController {
  constructor(private readonly service: VehicleService) {}

  @Post()
  add(@Body() body: any) {
    return this.service.add(body);
  }

  @Get()
  getAll() {
    return this.service.getAll();
  }

  @Get(':id')
  getById(@Param('id') id: string) {
    return this.service.getById(id);
  }

  @Patch(':id')
  updateById(@Param('id') id: string, @Body() body: Partial<Vehicle>) {
    return this.service.updateById(id, body);
  }

  @Delete(':id')
  removeById(@Param('id') id: string) {
    return this.service.removeById(id);
  }

  @Delete()
  removeAll() {
    return this.service.removeAll();
  }
}
