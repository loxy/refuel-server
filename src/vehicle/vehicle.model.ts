export interface Vehicle {
  id: string;
  name: string;
  mileageUnit: 'km' | 'mile';
  default: boolean;
}
