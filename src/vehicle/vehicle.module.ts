import { Module } from '@nestjs/common';
import { VehicleController } from './vehicle.controller';
import { VehicleService } from './vehicle.service';
import { DbModule } from '../db/db.module';

@Module({
  imports: [DbModule],
  controllers: [VehicleController],
  providers: [VehicleService],
})
export class VehicleModule {}
