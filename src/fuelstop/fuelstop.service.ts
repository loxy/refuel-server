import { BadRequestException, Injectable } from '@nestjs/common';

import { TABLE } from '../common/constants';
import { LowDb } from '../db/db-schema.model';
import { InjectDb } from '../db/inject-db.decorator';
import { Fuelstop } from './fuelstop.model';
import { generateId } from '../common/generate-id.fn';
import { CrudService } from '../db/crud.service';

@Injectable()
export class FuelstopService extends CrudService<Fuelstop> {
  constructor(@InjectDb() db: LowDb) {
    super(db, TABLE.STOPS);
  }

  add(stop: Partial<Fuelstop>) {
    const entry: Fuelstop = {
      id: generateId(),
      isFull: true,
      amountUnit: 'Liters',
      refueledOn: new Date(),
      vat: 19,
      ...stop,
      consumption: null,
      drivenSinceLastEntry: null,
      averageDrivenPerDay: null,
      netTotal: Math.round(stop.grossTotal / (stop.vat / 100 + 1)),
      pricePerUnit: Math.round(stop.grossTotal / stop.amount),
    } as Fuelstop;

    const prev = this.db
      .get(TABLE.STOPS)
      .last()
      .value() as Fuelstop;

    if (prev) {
      const drivenSinceLastEntry = stop.mileage - prev.mileage;
      if (drivenSinceLastEntry <= 1) {
        throw new BadRequestException(
          'mileage must be at least 1 unit more than before',
        );
      }
      entry.consumption = (stop.amount / drivenSinceLastEntry) * 100;
      entry.drivenSinceLastEntry = drivenSinceLastEntry;
    }

    return super.add(entry);
  }
}
