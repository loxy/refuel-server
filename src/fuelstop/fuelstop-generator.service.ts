import { Injectable } from '@nestjs/common';
import crypto from 'crypto';

import { generateRandomNumber } from '../common/generate-random-number.fn';
import { LowDb } from '../db/db-schema.model';
import { InjectDb } from '../db/inject-db.decorator';
import { Fuelstop, Refueling } from './fuelstop.model';
import { generateRandomInt } from '../common/generate-random-int.fn';
import { generateId } from '../common/generate-id.fn';

interface FuelstopGeneratorOptions {
  count: number;
  vehicleId: string | string[];
  vat: number;
  startMileage: number;
  maxDriven: number;
  consumptionsBounds: [number, number];
  pricePerUnitBounds: [number, number];
}

@Injectable()
export class FuelstopGeneratorService {
  constructor(@InjectDb() private readonly db: LowDb) {}

  generateStop(vehicleId: string) {
    const stop: Refueling = {
      amount: 12.15,
      isFull: true,
      grossTotal: 1250,
      id: crypto.randomBytes(3).toString('hex'), // todo: uuid über api besorgen
      mileage: 0,
      amountUnit: 'Liters',
      refueledOn: new Date(),
      vat: 19,
      vehicleId,
    };

    const prev = this.db
      .get('stops')
      .last()
      .value();

    if (prev) {
      stop.mileage = prev.mileage + Math.ceil(Math.random() * 250);
      stop.amount = generateRandomNumber(10, 50);
      stop.grossTotal = Math.round(
        stop.amount * generateRandomNumber(1.17, 1.45) * 100,
      );
    }

    return stop;
  }

  generateMultipleStops(options: FuelstopGeneratorOptions) {
    const {
      consumptionsBounds: [minConsumption, maxConsumption],
      pricePerUnitBounds: [minPrice, maxPrice],
    } = options;
    const stops: Fuelstop[] = [];
    for (let i = 0; i < options.count; i++) {
      const driven = generateRandomInt(30, options.maxDriven);
      const consumption = +generateRandomNumber(
        minConsumption,
        maxConsumption,
      ).toFixed(2);
      const pricePerUnit = generateRandomInt(minPrice, maxPrice);
      const amount = +((driven * consumption) / 100).toFixed(2);
      const stop: Fuelstop = {
        id: generateId(),
        amount,
        isFull: true,
        grossTotal: Math.round(pricePerUnit * amount),
        netTotal: Math.round((pricePerUnit * amount) / (options.vat / 100 + 1)),
        mileage: i === 0 ? driven : stops[i - 1].mileage + driven,
        amountUnit: 'Liters',
        refueledOn:
          i === 0
            ? new Date()
            : new Date(
                stops[i - 1].refueledOn.valueOf() +
                  1000 * 60 * 60 * 24 * generateRandomInt(2, 21),
              ),
        vat: options.vat,
        vehicleId: this.getVehicleId(options.vehicleId),
        consumption,
        drivenSinceLastEntry: driven,
        averageDrivenPerDay: null,
        pricePerUnit,
      };
      stops.push(stop);
    }
    return stops;
  }

  private getVehicleId(data: string | string[]): string {
    if (Array.isArray(data)) {
      return data[generateRandomInt(0, data.length)];
    }
    return data;
  }
}
