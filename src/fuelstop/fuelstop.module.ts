import { Module } from '@nestjs/common';
import { FuelstopController } from './fuelstop.controller';
import { FuelstopService } from './fuelstop.service';
import { FuelstopGeneratorService } from './fuelstop-generator.service';
import { DbModule } from '../db/db.module';

@Module({
  imports: [DbModule],
  controllers: [FuelstopController],
  providers: [FuelstopService, FuelstopGeneratorService],
})
export class FuelstopModule {}
