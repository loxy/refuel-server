import { IsNumber, IsNumberString } from 'class-validator';

export class FuelstopCreateDto {
  @IsNumber() @IsNumberString() amount: number;
  @IsNumber() @IsNumberString() mileage: number;
  @IsNumber() @IsNumberString() grossTotal: number;
}
