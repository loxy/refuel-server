import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Patch,
  Post,
} from '@nestjs/common';
import { FuelstopGeneratorService } from './fuelstop-generator.service';
import { Fuelstop } from './fuelstop.model';
import { FuelstopService } from './fuelstop.service';
import { FuelstopCreateDto } from './fuelstop-create.dto';

@Controller('api/stops')
export class FuelstopController {
  constructor(
    private readonly service: FuelstopService,
    private readonly generator: FuelstopGeneratorService,
  ) {}

  @Post('generate')
  generateStops(@Body() body) {
    const list = this.generator.generateMultipleStops({
      vat: 19,
      maxDriven: 650,
      vehicleId: '1',
      count: 10,
      startMileage: 0,
      consumptionsBounds: [3.8, 8.2],
      pricePerUnitBounds: [115, 138],
      ...body,
    });
    return this.service.addAll(list);
  }

  @Post()
  add(@Body() body: FuelstopCreateDto) {
    // return this.service.add(this.generator.generateStop('xxx'));
    return this.service.add(body);
  }

  @Get()
  getAll() {
    return this.service.getAll();
  }

  @Get(':id')
  getById(@Param('id') id: string) {
    return this.service.getById(id);
  }

  @Patch(':id')
  updateById(@Param('id') id: string, @Body() body: Partial<Fuelstop>) {
    return this.service.updateById(id, body);
  }

  @Delete(':id')
  removeById(@Param('id') id: string) {
    return this.service.removeById(id);
  }

  @Delete()
  removeAll() {
    return this.service.removeAll();
  }
}
