import { Vehicle } from '../vehicle/vehicle.model';

export interface Refueling {
  id: string;
  refueledOn: Date; // Getankt am
  vat: number; // Mehrwertsteuer
  grossTotal: number; // Brutto
  amount: number; // Menge
  amountUnit: 'Liters' | 'kWh' | 'Gallon';
  isFull: boolean; // Voller Tank?
  mileage: number; // Kilometerstand
  note?: string; // Notiz
  vehicleId?: string; // ID des zugeordneten Fahrzeugs
  vehicle?: Vehicle; // das zugeordnete Fahrzeug
}

export interface Fuelstop extends Refueling {
  netTotal: number; // Netto
  pricePerUnit: number; // Preis pro Einheit
  drivenSinceLastEntry: number; // gefahren seit letztem Tankstopp
  consumption: number; // Verbrauch
  averageDrivenPerDay: number;
}
