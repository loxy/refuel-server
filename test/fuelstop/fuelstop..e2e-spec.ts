import request from 'supertest';
import { Test } from '@nestjs/testing';
import { INestApplication } from '@nestjs/common';
import { AppModule } from '../../src/app.module';
import { PROVIDE } from '../../src/common/constants';
import { DbService } from '../../src/db/db.service';

describe('Cats', () => {
  let app: INestApplication;

  beforeAll(async () => {
    const module = await Test.createTestingModule({
      imports: [AppModule],
    })
      .overrideProvider(PROVIDE.DB)
      .useValue(new DbService('db-test').instance)
      .compile();

    app = module.createNestApplication();
    await app.init();
  });

  it(`/GET cats`, () => {
    return request(app.getHttpServer())
      .get('/api/stops')
      .expect(200)
      .expect([]);
  });

  afterAll(async () => {
    await app.close();
  });
});
